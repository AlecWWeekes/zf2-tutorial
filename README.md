Zend 2 Framework - Introduction Development
=======================

Introduction
------------
This is the culmination of the development with the Zend Tutorial Code.

This includes both the Album & Blog Modules we've worked on throughout
my probation period. This should be used as a helpful guide, but is merely
a starting point.

Why keep this?
------------
In a couple of weeks, months or years no doubt I'll look back on this project
and realise just how much I've grown and come along. As with all development
I'll look back and realise just how easy it was really, but how tough
I found it working with a new concept.

This is a piece of my developmental history. This is the first MVC project,
and my first Zend project. This is the first time I've also made serious use
of Object Orientated Programming (OOP), and so no doubt I'll have a good 
laugh when I look back on just how terrible I was.

> Alec 'Hyve' Weekes (12th December 2017 @ Symbios)

<?php

	namespace Blog\Form;

	use Zend\Form\Form;

	class PostForm extends Form {

		/**
		 * 							[IMPORTANT]
		 *
		 * 	When working with Zend\Form we *MUST* use the following;
		 *
		 * 		[1]	Set the Constructor to have these settings!
		 * 		[2] Set the Parent Constructor to also have the settings
		 *
		 * 	Without these then the form will return an error similar to;
		 * 	
		 * 		Fatal error: Call to a member function insert() on a non-object in
 		 * 		{libraryPath}/Zend/Form/Fieldset.php on line {lineNumber}
 		 *
 		 *  If you ever get an issue when using forms, do this!
		 */

		public function __construct($name = null, $options = array()) {

			parent::__construct($name, $options);

			$this->add(array(

				'name' => 'post-fieldset',
				'type' => 'Blog\Form\PostFieldset',
				'options' => array(
					'use_as_base_fieldset' => true,
				),

			));

			$this->add(array(

				'type' => 'submit',
				'name' => 'submit',
				'attributes' => array(
					'value'	 => 'Insert new Post',
				)

			));

		}

	}
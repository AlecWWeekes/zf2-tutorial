<?php

	namespace Blog\Form;

	use Blog\Model\Post;
	use Zend\Form\Fieldset;
	use Zend\Stdlib\Hydrator\ClassMethods;

	class PostFieldset extends Fieldset {

		/**
		 * 							[IMPORTANT]
		 *
		 * 	When working with Zend\Form we *MUST* use the following;
		 *
		 * 		[1]	Set the Constructor to have these settings!
		 * 		[2] Set the Parent Constructor to also have the settings
		 *
		 * 	Without these then the form will return an error similar to;
		 * 	
		 * 		Fatal error: Call to a member function insert() on a non-object in
 		 * 		{libraryPath}/Zend/Form/Fieldset.php on line {lineNumber}
 		 *
 		 *  If you ever get an issue when using forms, do this!
		 */

		public function __construct($name = null, $options = array()) {

			parent::__construct($name, $options);

			$this->setHydrator(new ClassMethods(false));
			$this->setObject(new Post());

			$this->add(array(
				'type'	=> 'hidden',
				'name' 	=> 'id',

			));

			$this->add(array(

				'type'	=> 'text',
				'name' 	=> 'title',
				'options' 	=> array(
					'label' => 'Blog Title',
				)

			));

			$this->add(array(

				'type'	=> 'text',
				'name' 	=> 'text',
				'options' 	=> array(
					'label' => 'The Text',
				)

			));
		}

	}
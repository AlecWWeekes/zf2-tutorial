<?php

	namespace Blog\Mapper;

	use Blog\Model\PostInterface;
	use Zend\Db\Adapter\AdapterInterface;
	use Zend\Db\Adapter\Driver\ResultInterface;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Sql;
	use Zend\Db\Sql\Insert;
	use Zend\Db\Sql\Update;
	use Zend\Db\Sql\Delete;
	use Zend\Stdlib\Hydrator\HydratorInterface;

	class ZendDbSqlMapper implements PostMapperInterface {

		protected $dbAdapter;
		protected $hydrator;
		protected $postPrototype;

		public function __construct(AdapterInterface $dbAdapter, HydratorInterface $hydrator, PostInterface $postPrototype) {

			$this->dbAdapter 		= $dbAdapter;
			$this->hydrator 		= $hydrator;
			$this->postPrototype 	= $postPrototype;

		}	

		public function find($id) {

			$sql    = new Sql($this->dbAdapter);
	        $select = $sql->select('posts');
	        $select->where(array('id = ?' => $id));

	        $stmt   = $sql->prepareStatementForSqlObject($select);
	        $result = $stmt->execute();

	        if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
	            return $this->hydrator->hydrate($result->current(), $this->postPrototype);
	        }

	        throw new \InvalidArgumentException("Blog with given ID:{$id} not found.");
     	}


		public function findAll() {

			$sql 	= new Sql($this->dbAdapter);
			$select = $sql->select('posts');		// Select the table!

			$stmt	= $sql->prepareStatementForSqlObject($select);
			$result = $stmt->execute();

			if ($result instanceof ResultInterface && $result->isQueryResult()) {

				$resultSet = new HydratingResultSet($this->hydrator, $this->postPrototype);

				return $resultSet->initialize($result);

			}

			return array();

		}

		public function save(PostInterface $postObject) {

			$postData = $this->hydrator->extract($postObject);

			unset($postData['id']);

			/**
			 * 	The following If Statement just prepares the relevant Class and Data for it's needed role.
			 * 	So, for example, this is checking if it has an ID, if it does, then this is clearly an
			 * 	update so prepare the Update() Class.
			 *
			 * 	If there is no ID, then use the Insert() class instead.
			 *
			 * 	None of this statement executes the SQL functions. This is all just assignment and preperation.
			 */
			
			if ($postObject->getId()) {

				// If an ID is present, do an Update!

				
				$action = new Update('posts');								// This Update() Class comes from Zend\Db\Sql\Update; which we're using at the top!
				$action->set($postData);									// Set the $postData
				$action->where(array('id = ?' => $postObject->getId()));	// Where SQL Statement

			} else {

				// If no ID is present, do an Insert!
				
				$action = new Insert('posts');								// This Insert() Class comes from Zend\Db\Sql\Insert; which we're using at the top!
				$action->values($postData);									

			}

			$sql 	= new Sql($this->dbAdapter);							// Build a new SQL Class using the dbAdapter which has our Database Details!
			$stmt 	= $sql->prepareStatementForSqlObject($action);			// This is identical to the MySQLi Prepare PDO Statement.
			$result = $stmt->execute();										// Execute the SQL!

			if ($result instanceof ResultInterface) {

				if ($newId = $result->getGeneratedValue()) {

					$postObject->setId($newId);

				}

				return $postObject;

			}

			throw new \Exception("Database Error");

		}

		public function delete(PostInterface $postObject) {

			$action = new Delete('posts');
			$action->where(array('id = ?' => $postObject->getId()));

			$sql 	= new Sql($this->dbAdapter);
			$stmt 	= $sql->prepareStatementForSqlObject($action);
			$result = $stmt->execute();

			return (bool)$result->getAffectedRows();

		}

	}
<?php

	namespace Blog\Model;

	class Post implements PostInterface {

		protected $id;
		protected $title;
		protected $text;

		/**
		 * 	ID Functions (Get / Set)
		 */

		// See Post Interface for functionality
		public function getId() {

			return $this->id;

		}

		public function setId($id) {

			$this->id = $id;

		}

		/**
		 * 	Title Functions (Get / Set)
		 */
		
		public function getTitle() {

			return $this->title;

		}

		public function setTitle($title) {

			$this->title = $title;

		}

		/**
		 * 	Text Fucntions (Get / Set)
		 */
		
		public function getText() {

			return $this->text;

		}

		public function setText($text) {

			$this->text = $text;

		}

	}
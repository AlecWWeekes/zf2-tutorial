<?php

	namespace Blog\Model;

	interface PostInterface {

		/**
		 * 	Will return the ID of the post
		 *
		 * 	@return 	Int
		 */

		public function getId();

		/**
		 * 	Will return the Title of the post
		 *
		 * 	@return 	String
		 */

		public function getTitle();

		/**
		 *	Will return the Text of the post
		 *
		 * 	@return   	String
		 */

		public function getText();

	}
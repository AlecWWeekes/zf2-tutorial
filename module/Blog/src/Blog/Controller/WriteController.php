<?php

	namespace Blog\Controller;

	use Blog\Service\PostServiceInterface;
	use Zend\Form\FormInterface;
	use Zend\Mvc\Controller\AbstractActionController;
	use Zend\View\Model\ViewModel;

	class WriteController extends AbstractActionController {

		protected $postService;
		protected $postForm;

		public function __construct(PostServiceInterface $postService, FormInterface $postForm) {

			$this->postService 	= $postService;
			$this->postForm 	= $postForm;

		}

		public function addAction() {


			// Get the request data
			$request = $this->getRequest();

			// Is this a POST Method (Source: https://framework.zend.com/manual/1.10/en/zend.controller.request.html)
			if ($request->isPost()) {

				/**
				 * 	If our request was done via the POST method, now use the
				 * 	setData() function to attach the data to our postForm.
				 */

				$this->postForm->setData($request->getPost());

				if ($this->postForm->isValid()) {

					try {

						$this->postService->savePost($this->postForm->getData());

						//\Zend\Debug\Debug::dump($this->postForm->getData());die();

						return $this->redirect()->toRoute('blog');

					} catch (\Exception $e) {

						throw new \Exception("There has been a database issue! Please immediately check this file!");
						die();

					}

				}

			}

			return new ViewModel(array(
				'form' => $this->postForm
			));

		}

		public function editAction() {

			$request 	= $this->getRequest();
			$post 		= $this->postService->findPost($this->params('id'));

			$this->postForm->bind($post);

			if ($request->isPost()) {

				$this->postForm->setData($request->getPost());

				if ($this->postForm->isValid()) {

					try {

						$this->postService->savePost($post);
						return $this->redirect()->toRoute('blog');

					} catch (\Exception $e) {

						throw new \Exception("There has been a database issue! Please immediately check this file!");
						die();

					}

				}

			}

			// This "Form" can be found inside our View. We create a Form Variable there, and use this data from there!
			return new ViewModel(array(
				'form' => $this->postForm
			));

		}

	}
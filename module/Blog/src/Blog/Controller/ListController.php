<?php

	namespace Blog\Controller;

	use Blog\Service\PostServiceInterface;
	use Zend\View\Model\ViewModel;
	use Zend\Mvc\Controller\AbstractActionController;
	

	class ListController extends AbstractActionController {

		protected $postService;

		public function __construct(PostServiceInterface $postService) {

			$this->postService = $postService;

		}

		public function indexAction() {

			return new ViewModel(array(

				'posts' => $this->postService->findAllPosts()

			));

		}

		public function detailAction() {

			// Get the ID from the route we're using. Kinda like getting something from the URL
			$id = $this->params()->fromRoute('id');

			try {

				return new ViewModel(array(
					'post' => $this->postService->findPost($id)
				));

			} 

			catch (\InvalidArgumentException $ex) {
				return $this->redirect()->toRoute('blog');
			}

		}

	}
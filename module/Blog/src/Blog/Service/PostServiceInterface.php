<?php

	namespace Blog\Service;

	use Blog\Model\PostInterface;

	interface PostServiceInterface {

		/**
		 * 	Will return all the blog posts
		 *
		 * 	@return   Array | PostInterface
		 */

		public function findAllPosts();

		/**
		 * 	Will return a single blog post.
		 *
		 * 	@param   Integer ID for the post itself
		 * 	@return  PostInterface
		 */
		
		public function findPost($id);

		public function savePost(PostInterface $post);

		public function deletePost(PostInterface $post);

	}
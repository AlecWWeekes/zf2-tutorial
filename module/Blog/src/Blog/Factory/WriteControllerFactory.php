<?php

	namespace Blog\Factory;

	use Blog\Controller\WriteController;
	use Zend\ServiceManager\FactoryInterface;
	use Zend\ServiceManager\ServiceLocatorInterface;

	class WriteControllerFactory implements FactoryInterface {

		public function createService(ServiceLocatorInterface $serviceLocator) {

			$realServiceLocator = $serviceLocator->getServiceLocator();
			$postService 		= $realServiceLocator->get('Blog\Service\PostServiceInterface');

			// When handling forms, we almost always want to use the FormElementManager
			$postInsertForm		= $realServiceLocator->get('FormElementManager')->get('Blog\Form\PostForm');

			return new WriteController($postService, $postInsertForm);

		}

	}
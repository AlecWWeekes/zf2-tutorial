<?php

	/**
	 * 	This is the ListController Factory. As we are using our ListController with
	 * 	Dependency Injection, we're going to need to convert from an Invokable to a
	 * 	Factory, which is what this Class will be doing!
	 *
	 * 	Source: https://framework.zend.com/manual/2.4/en/in-depth-guide/services-and-servicemanager.html#writing-a-factory-class
	 */
	
	namespace Blog\Factory;

	use Blog\Controller\ListController;
	use Zend\ServiceManager\FactoryInterface;
	use Zend\ServiceManager\ServiceLocatorInterface;

	class ListControllerFactory implements FactoryInterface {

		public function createService(ServiceLocatorInterface $serviceLocator) {

			$realServiceLocator = $serviceLocator->getServiceLocator();
			$postService 		= $realServiceLocator->get('Blog\Service\PostServiceInterface');

			return new ListController($postService);

		}

	}
<?php 

	return array(

		/**
		 *
		 *		[GIT TEST]
		 * 
		 */

		'view_manager' => array(
	        'template_path_stack' => array(
	            __DIR__ . '/../view',
	        ),
	    ),

	    'controllers' 		=> array(
	    	'factories' 	=> array(
	    		'Blog\Controller\List' 					=> 'Blog\Factory\ListControllerFactory',
	    		'Blog\Controller\Write' 				=> 'Blog\Factory\WriteControllerFactory',
	    		'Blog\Controller\Delete'				=> 'Blog\Factory\DeleteControllerFactory',
	    	),
	    ),

	    'service_manager' 	=> array(
	    	'factories'		=> array(
	    		'Blog\Mapper\PostMapperInterface' 		=> 'Blog\Factory\ZendDbSqlMapperFactory',
	    		'Blog\Service\PostServiceInterface' 	=> 'Blog\Factory\PostServiceFactory',
	    		'Zend\Db\Adapter\Adapter' 				=> 'Zend\Db\Adapter\AdapterServiceFactory',
	    	),
	    ),

	    'router' => array(
	        'routes' => array(
	            'blog' => array(
	                'type' => 'literal',
	                'options' => array(
	                    'route'    => '/blog',
	                    'defaults' => array(
	                        'controller' => 'Blog\Controller\List',
	                        'action'     => 'index',
	                    ),
	                ),
	                'may_terminate' => true,
	                'child_routes'  => array(

	                	// Details / View Blog Post Route
	                    'detail' => array(
	                        'type' => 'segment',
	                        'options' => array(
	                            'route'    => '/:id',
	                            'defaults' => array(
	                                'action' => 'detail'
	                            ),
	                            'constraints' => array(
	                                'id' => '[1-9]\d*'
	                            )
	                        )
	                    ),

	                    // Add Blog Post Route
	                    'add' => array(
	                        'type' => 'literal',
	                        'options' => array(
	                            'route'    => '/add',
	                            'defaults' => array(
	                                'controller' => 'Blog\Controller\Write',
	                                'action'     => 'add'
	                            )
	                        )
                    	),

                    	// Edit Blog Post Route
	                    'edit' => array(
	                    	'type' 		=> 'segment',
	                    	'options' 	=> array(
	                    		'route' => '/edit/:id',
	                    		'defaults' => array(
	                    			'controller' 	=> 'Blog\Controller\Write',
	                    			'action' 		=> 'edit'
	                    		),
	                    		'constraints' => array(
	                    			'id' => '\d+'
	                    		),
	                    	)
	                    ),

	                    // Edit Blog Post Route
	                    'delete' => array(
	                    	'type' 		=> 'segment',
	                    	'options' 	=> array(
	                    		'route' => '/delete/:id',
	                    		'defaults' => array(
	                    			'controller' 	=> 'Blog\Controller\Delete',
	                    			'action' 		=> 'delete'
	                    		),
	                    		'constraints' => array(
	                    			'id' => '\d+'
	                    		),
	                    	)
	                    ),
	                )
	            )
	        )
	    )
	);
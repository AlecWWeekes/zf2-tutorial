<?php

	namespace Album;

	use Album\Model\Album;
	use Album\Model\AlbumTable;

	use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
 	use Zend\ModuleManager\Feature\ConfigProviderInterface;
	use Zend\Db\ResultSet\ResultSet;
	use Zend\Db\TableGateway\TableGateway;

	// We implement the Interfaces, which mean we will NEED to use the functions they've built!
	class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

		public function getAutoloaderConfig() {

			/**
			 *	[IMPORTANT]
			 * 	
			 *	We've changed how we use this file. Typically what you see below is how we'd handle
			 *	things however because we're using Composer we're going to be a little crazy and
			 *	use the autoload functionality withing the PSR-0
			 *
			 * 	To do that, we just leave this function blank and follow the instructions in the
			 * 	source
			 *
			 * 	Source: https://framework.zend.com/manual/2.4/en/user-guide/modules.html#autoloading-files
			 */
			 

			return array (

				// By default this should sit in the root directory of our Module, but we've moved
				// it to the config because that's just nicer and makes more sense!
				'Zend\Loader\ClassMapAutoloader' => array(
					__DIR__ . '/config/autoload_classmap.php',
				),

				// This will automatically load the namespaces based on their directories!
				'Zend\Loader\StandardAutoloader' => array(
					'namespaces' => array(
						__NAMESPACE__ => __DIR__ . "/src" . __NAMESPACE__,
					),
				),
			);

		}

		public function getConfig() {

			// Very simple require_once occuring. Nothing special!
			return require_once __DIR__ . '/config/module.config.php';

		}

		public function getServiceConfig() {

			return array(

				'factories' => array(
					
					'Album\Model\AlbumTable' => function($sm) {

						$tableGateway = $sm->get('AlbumTableGateway');
						$table = new AlbumTable($tableGateway);
						return $table;

					},

					'AlbumTableGateway' => function ($sm) {

						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
						$resultSetPrototype = new ResultSet();
						$resultSetPrototype->setArrayObjectPrototype(new Album());
						return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);

					},

				),

			);

		}

	}
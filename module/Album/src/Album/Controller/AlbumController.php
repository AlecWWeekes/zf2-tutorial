<?php

	namespace Album\Controller;

	use Zend\Mvc\Controller\AbstractActionController;		// This is clearly a Controller (Logic Handler)
	use Zend\View\Model\ViewModel;							// This is clearly a Model (Data Handler)
	use Album\Model\Album;
	use Album\Form\AlbumForm;

	// Remember in our Config, we're calling this as Album, and this is the default Controller!
	class AlbumController extends AbstractActionController {

		protected $albumTable;

		// As detailed in our Route, this the the default Action
		public function indexAction() {

			return new ViewModel(array(

				'albums' => $this->getAlbumTable()->fetchAll(),

			));

		}

		public function getAlbumTable() {

			if( !$this->albumTable ) {

				$sm = $this->getServiceLocator();
				$this->albumTable = $sm->get('Album\Model\AlbumTable');

			}

			return $this->albumTable;

		}

		public function addAction() {

			$form = new AlbumForm();
			$form->get('submit')->setValue('Add');

			$request = $this->getRequest();

			if ($request->isPost()) {

				$album = new Album();

				$form->setInputFilter($album->getInputFilter());
				$form->setData($request->getPost());

				if($form->isValid()) {

					$album->exchangeArray($form->getData());
					$this->getAlbumTable()->saveAlbum($album);

					return $this->redirect()->toRoute('album');

				}

			}

			return array('form' => $form);

		}

		public function editAction() {

			$id = (int) $this->params()->fromRoute('id', 0);

			// If the ID from the route isn't set, redirect the user to Add this ID (and thus Album)
			if (!$id) {

				return $this->redirect()->toRoute('album', array('action' => 'add'));

			}

			try {

				$album = $this->getAlbumTable()->getAlbum($id);

			} catch (\Exception $ex) {

				return $this->redirect()->toRoute('album', array('action' => 'index'));

			}

			$form = new AlbumForm();
			$form->bind($album);
			$form->get('submit')->setAttribute('value', 'Edit');

			$request = $this->getRequest();
			if ($request->isPost()) {

				$form->setInputFilter($album->getInputFilter());
				$form->setData($request->getPost());

				if ($form->isValid()) {

					$this->getAlbumTable()->saveAlbum($album);

					return $this->redirect()->toRoute('album');
				}

			}

			return array ('id' => $id, 'form' => $form);

		}

		public function deleteAction() {

			$id = (int) $this->params()->fromRoute('id', 0);

			if (!$id) {

				return $this->redirect()->toRoute('album');

			}

			$request = $this->getRequest();

			if ($request->isPost()) {

				$del = $request->getPost('del', 'No');

				if ($del == 'Yes') {

					$id = (int) $request->getPost('id');
					$this->getAlbumTable()->deleteAlbum($id);

				}

				return $this->redirect()->toRoute('album');

			}

			return array(

				'id' => $id,
				'album' => $this->getAlbumTable()->getAlbum($id),

			);

		}

	}


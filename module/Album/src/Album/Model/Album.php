<?php

	namespace Album\Model;

	use Zend\InputFilter\InputFilter;
	use Zend\InputFilter\InputFilterAwareInterface;
	use Zend\InputFilter\InputFilterInterface;

	class Album implements InputFilterAwareInterface {

		public $id;
		public $artist;
		public $title;

		protected $inputFilter;

		public function exchangeArray($data) {

			$this->id = (!empty($data['id'])) ? $data['id'] : null;
			$this->artist = (!empty($data['artist'])) ? $data['artist'] : null;
			$this->title = (!empty($data['title'])) ? $data['title'] : null;

		}

          public function getArrayCopy() {

               return get_object_vars($this);
               
          }


		// These methods (Functions) are related to the AlbumForm!
		public function setInputFilter(InputFilterInterface $inputFilter) {

        	throw new \Exception("This function is mandatory, but never used!");

     	}

     	public function getInputFilter() {

     		if (!$this->inputFilter) {

     			// We've just created a new class for InputFiler, using the InputFilter from Zend!
     			$inputFilter = new InputFilter();

     			// Add Input Filter for ID
     			$inputFilter->add(array(

     				'name' => 'id',
     				'required' => true,
     				'filters' => array(
     					array('name' => 'Int'),
     				),

     			));

     			// Add input Filter for Artist Name
     			$inputFilter->add(array(

     				'name' => 'artist',
     				'required' => true,
     				'filters' => array(
     					array('name' => 'StripTags'),
     					array('name' => 'StringTrim'),
     				),
     				'validators' => array(
     					array(
     						'name' => 'StringLength',
     						'options' => array(
     							'encoding' => 'UTF-8',
     							'min' => 1,
     							'max' => 100,
     						),
     					),
     				)

     			));

     			// Add input Filter for Title Name
     			$inputFilter->add(array(

     				'name' => 'title',
     				'required' => true,
     				'filters' => array(
     					array('name' => 'StripTags'),
     					array('name' => 'StringTrim'),
     				),
     				'validators' => array(
     					array(
     						'name' => 'StringLength',
     						'options' => array(
     							'encoding' => 'UTF-8',
     							'min' => 1,
     							'max' => 100,
     						),
     					),
     				)

     			));

     			$this->inputFilter = $inputFilter;

     		}

     		return $this->inputFilter;

     	}


	}
<?php

	namespace Album\Model;

	use Zend\Db\TableGateway\TableGateway;

	class AlbumTable {

		protected $tableGateway;

		public function __construct(TableGateway $tableGateway) {

			$this->tableGateway = $tableGateway;

		}

		public function fetchAll() {

			$resultSet = $this->tableGateway->select();

			return $resultSet;

		}

		public function getAlbum($id) {

			$id = (int) $id;

			$rowset = $this->tableGateway->select(array('id' => $id));

			$row = $rowset->current();

			if (!$row) {

				throw new \Exception("Could not locate row by the ID of $id");

			}

			return $row;

		}

		public function saveAlbum(Album $album) {

			$data = array(

				'artist'	=> $album->artist,
				'title'		=> $album->title,

			);

			$id = (int) $album->id;

			// If the $album->id is 0, then it doesn't exist in the database yet.
			if ($id == 0) {

				// Because it doesn't exist yet, let's add it to the Database!
				$this->tableGateway->insert($data);

			}
			else {

				if ($this->getAlbum($id)) {

					// Update with the $data based on the array ID
					$this->tableGateway->update($data, array('id' => $id));

				}
				// If the Album ID wasn't 0, but can't be found in the Database, throw an exception
				else {

					throw new \Exception("The Album ID doesn't appear the exist in the Database!");

				}

			}

		}

		public function deleteAlbum($id) {

			$this->tableGateway->delete(array('id' => (int) $id));

		}

	}
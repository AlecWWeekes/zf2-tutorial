<?php

	return array(

		// Controllers
		'controllers' => array(
			'invokables' => array(
				// We're just going to refer to AlbumController in Controller, as Album!
				'Album\Controller\Album' => 'Album\Controller\AlbumController',
			),
		),

		'router' => array(
			'routes' => array(

				// Name of the Route. You can have more than one Route!
				'album' => array(
					'type' 		=> 'segment',
					'options' 	=> array(
						'route'		=> '/album[/:action][/:id]',

						'constraints' 	=> array(
							'action'		=> '[a-zA-Z][a-zA-Z0-9_-]*',
							'id'			=> '[0-9]+',
						),

						'defaults' 		=> array(
							'controller' 	=> 'Album\Controller\Album',		// Remember, this is the same as Album\Controller\AlbumController as we changed this in the Controller Sections above!
							'action'		=> 'index',
						),
					),
				),

				// New Route would go here!
			),
		),

		// View Manager - Set the directory for the Views!
		'view_manager' => array(
			'template_path_stack' => array(
				'album' => __DIR__ . '/../view',
			),
		),

	);